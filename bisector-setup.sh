#!/bin/sh

set -eu

RELEASE=${RELEASE:-$(lsb_release -c -s)}
ARCH="${ARCH:-$(dpkg --print-architecture)}"
REPO_PATH=${REPO_PATH:-r/$RELEASE}
export REPREPRO_BASE_DIR="${REPREPRO_BASE_DIR:-$HOME/public_html/$REPO_PATH}"
if ubuntu-distro-info --series $RELEASE > /dev/null 2>&1; then
    REPREPRO_COMPONENTS="${REPREPRO_COMPONENTS:-main restricted universe multiverse}"
    DISTRO=ubuntu
else
    REPREPRO_COMPONENTS="${REPREPRO_COMPONENTS:-main contrib non-free}"
    DISTRO=debian
    # remap release because mk-sbuild does that, too
    case $RELEASE in
        unstable)
            RELEASE=sid
            ;;
        *)
    esac
fi

LOCAL_IP=${LOCAL_IP:-$(ip add | awk '/ inet 127\.0\.0\./ {next} / inet / {gsub("/.*","",$2);print $2; exit}')}
MIRROR=${MIRROR:-$(awk '/^deb /{print $2; exit}' /etc/apt/sources.list)}

if [ -z ${http_proxy:-} ] ; then
    http_proxy=$(apt-config dump | awk 'BEGIN {FS="\""} /Acquire::http::Proxy / {print $2}')
    if [ -z ${http_proxy:-} ] ; then
        # use first non-localhost local address
        http_proxy=http://$LOCAL_IP:8000
        sudo apt-get install -y squid-deb-proxy
    fi
fi

apt-config dump | grep -q Proxy || echo "Acquire::http::Proxy \"$http_proxy\";" | sudo tee /etc/apt/apt.conf.d/91bisector-aptproxy

### Set up sbuild with eatmydata and ccache

# sendmail is for mk-sbuild
sudo apt-get install -y ubuntu-dev-tools sbuild ccache sendmail

if [ -f /etc/apt/apt.conf.d/91bisector-aptproxy ] && ! grep -q bisector-aptproxy /etc/schroot/sbuild/copyfiles; then
    echo /etc/apt/apt.conf.d/91bisector-aptproxy | sudo tee -a /etc/schroot/sbuild/copyfiles > /dev/null
fi

# from https://wiki.debian.org/sbuild

dir=/var/cache/ccache-sbuild/$RELEASE-$ARCH
sudo install --group=sbuild --mode=2775 -d $dir
sudo env CCACHE_DIR=$dir ccache --max-size 16G
sudo env CCACHE_DIR=$dir ccache -o compiler_check=content
sudo env CCACHE_DIR=$dir ccache -o hash_dir=false
grep -q ^$dir /etc/schroot/sbuild/fstab || sudo tee -a /etc/schroot/sbuild/fstab <<END
$dir $dir none rw,bind 0 0
END

sudo tee $dir/sbuild-setup <<END
#!/bin/sh
export CCACHE_DIR=$dir
export CCACHE_UMASK=002
export CCACHE_COMPRESS=1
unset CCACHE_HARDLINK
export PATH="/usr/lib/ccache:\$PATH"
exec eatmydata "\$@"
END

sudo chmod a+rx $dir/sbuild-setup

groups $USER | grep -q sbuild || sudo adduser $USER sbuild
sudo -u $USER env EDITOR=touch mk-sbuild --distro=$DISTRO --skip-proposed --arch=$ARCH --debootstrap-proxy=$http_proxy --eatmydata --debootstrap-include=ccache $RELEASE

sbuild_config=/etc/schroot/chroot.d/sbuild-$RELEASE-$ARCH
grep -q 'command-prefix=.*ccache' $sbuild_config || sudo sed -i "s|command-prefix=.*|command-prefix=$dir/sbuild-setup|" $sbuild_config

sudo sbuild-adduser $USER

### Set up Apache2 and reprepro for serving built packages locally

sudo apt-get install -y apache2 reprepro
sudo a2enmod userdir
sudo service apache2 restart

mkdir -p $REPREPRO_BASE_DIR/conf
cat > $REPREPRO_BASE_DIR/conf/distributions <<EOF
Codename: $RELEASE
Components: $REPREPRO_COMPONENTS
UDebComponents: $REPREPRO_COMPONENTS
Architectures: source $ARCH
EOF

reprepro export

### Set up autopkgtest image

sudo apt-get install -y autopkgtest qemu-system

sudo adduser $USER kvm

if ubuntu-distro-info --series $RELEASE > /dev/null 2>&1; then
    # VM can run all autopkgtest
    LOCAL_REPO="[trusted=yes] http://$LOCAL_IP/~$USER/$REPO_PATH $RELEASE $REPREPRO_COMPONENTS"
    sudo -u $USER -- autopkgtest-buildvm-ubuntu-cloud -r $RELEASE -p $http_proxy -a $ARCH -m $MIRROR -s 40G --post-command="echo deb $LOCAL_REPO > /etc/apt/sources.list.d/local.list; echo deb-src $LOCAL_REPO >> /etc/apt/sources.list.d/local.list; echo Acquire::http::proxy::$LOCAL_IP \\"DIRECT\\"; > /etc/apt/apt.conf.d/91local-direct.conf"
else
    echo "TODO: set up autopkgtest for non Ubuntu derivatives" >&2
    exit 0
fi
